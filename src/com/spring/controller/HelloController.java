package com.spring.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
public class HelloController {


    @RequestMapping("/")
    public ModelAndView firstPage() {
        ModelAndView modelAndView = new ModelAndView("../../index");
        return modelAndView;
    }

    @RequestMapping("/welcome")
    public ModelAndView welcomeWorld() {
        ModelAndView modelAndView = new ModelAndView("start");
        modelAndView.addObject("message","Welcome in the Spring  World!");
        return modelAndView;
    }

    @RequestMapping("/hello")
    public ModelAndView hello() {
        ModelAndView modelAndView = new ModelAndView("start");
        modelAndView.addObject("message","Hello World!");
        return modelAndView;
    }

//    @RequestMapping("/welcome/{country}/{name}")
//    public ModelAndView pathVariable(@PathVariable("name") String name,@PathVariable("country") String country) {
//        ModelAndView modelAndView = new ModelAndView("start");
//        modelAndView.addObject("message","Path Variable "+ name +"  "+country);
//        return modelAndView;
//    }

    @RequestMapping("/welcome/{country}/{name}")
    public ModelAndView pathVariableMap(@PathVariable Map<String,String> pathVars) {
        String country = pathVars.get("country");
        String name = pathVars.get("name");

        ModelAndView modelAndView = new ModelAndView("start");
        modelAndView.addObject("message",name + " is from  " + country);
        return modelAndView;
    }
}
